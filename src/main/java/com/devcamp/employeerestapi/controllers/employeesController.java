package com.devcamp.employeerestapi.controllers;

import java.util.ArrayList;

import org.springframework.boot.autoconfigure.cache.CacheProperties.Couchbase;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.employeerestapi.models.Employee;

@RestController
@RequestMapping("/employees")
public class employeesController {
    @GetMapping
    public ArrayList<Employee> getEmployees() {
        ArrayList<Employee> employees = new ArrayList<>();

        // Tạo danh sách các đối tượng Employee
        employees.add(new Employee(1, "John", "Doe", 50000));
        employees.add(new Employee(2, "Jane", "Smith", 60000));
        employees.add(new Employee(3, "Mike", "Johnson", 70000));

        for (Employee employee : employees) {
            System.out.println(employee.toString());
        }

        return employees;
    }

}

// public static void main(String[] args) {
//     Employee employee1 = new Employee(1, "John", "Doe", 50000);
//     Employee employee2 = new Employee(2, "Jane", "Smith", 60000);
//     Employee employee3 = new Employee(3, "Mike", "Johnson", 70000);

//     System.out.println(employee1.toString());
//     System.out.println(employee2.toString());
//     System.out.println(employee3.toString());
// }
